<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('ecommerce')->group(function() {
            Route::name('ecommerce.')->group(function() {

                Route::get('dashboard', 'Admin\EcommerceController@index')->name('dashboard');//->middleware('permission:ecommerce-dashboard-read');

                Route::prefix('product-category')->group(function() {
                    Route::name('product-category.')->group(function() {
                        Route::get('/', 'Admin\ProductCategoryController@index')->name('index');
                    });
                });

            });
        });
    });
});
