<?php

use App\Common\CustomBlueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Modules\Ecommerce\Entities\EcommerceModel;

class CreateProductCategoriesTable extends Migration
{
    protected $tablePrefix = EcommerceModel::DB_TABLE_PREFIX;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $schema = DB::connection()->getSchemaBuilder();

        $schema->blueprintResolver(function ($table, $callback) {
            return new CustomBlueprint($table, $callback);
        });

        $schema->create($this->tablePrefix . 'product_categories', function (CustomBlueprint $table) {
            $table->uuid('id');
            $table->string('name', 100)->unique();
            $table->string('slug', 100)->unique();
            $table->text('description')->nullable();
            $table->uuid('parent_id')->nullable();
            $table->string('image', 40)->nullable();
            $table->commonFields();

            $table->primary('id');

            $table->foreign('parent_id')
                ->references('id')
                ->on($this->tablePrefix . 'product_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablePrefix . 'product_categories');
    }
}
