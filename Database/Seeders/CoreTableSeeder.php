<?php

namespace Modules\Ecommerce\Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Master\Role;
use Illuminate\Database\Seeder;
use App\Models\Master\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class CoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            'ecommerce',
            'ecommerce-dashboard-read',
            'product-category-read', 'product-category-create', 'product-category-edit', 'product-category-delete',
            'product-read', 'product-create', 'product-edit', 'product-delete', 'product-publish', 'product-archive',
        ];

        foreach($permissions as $row) {
            $permission = Permission::where('name', $row)->first();
            if($permission) {
                $this->command->info('Permission ' .$row. ' already exists.');
            } else {
                Permission::create([
                    'name' => $row
                ]);
                $this->command->info('Permission ' .$row. ' created successfully.');
            }
        }

        $roles = ['com-owner', 'com-admin-toko', 'com-customer', 'com-reseller', 'com-dropshipper'];

        foreach($roles as $row) {
            $role = Role::where('name', $row)->first();
            if($role) {
                $this->command->info('Role ' .$row. ' already exists.');
                $thisRole = $role;
            } else {
                $newRole = Role::create([
                    'name' => $row
                ]);
                $this->command->info('Role ' .$row. ' created successfully.');
                $thisRole = $newRole;
            }

            switch($row) {
                case 'com-owner':
                    $thisRole->givePermissionTo($permissions);
                    break;
                case 'com-admin-toko':
                    $thisRole->givePermissionTo([
                        'ecommerce',
                        'ecommerce-dashboard-read',
                        'product-category-read', 'product-category-create', 'product-category-edit', 'product-category-delete',
                        'product-read', 'product-create', 'product-edit', 'product-delete', 'product-publish', 'product-archive',
                    ]);
                    break;
            }
            $this->command->info('Role ' . $row .' syncronized with permissions successfully.');
        }

        /**
         * Create default user
         */

        $users = [
            ['name' => 'Owner', 'email' => 'owner@demo.com', 'role' => 'com-owner'],
            ['name' => 'Admin Toko', 'email' => 'admintoko@demo.com', 'role' => 'com-admin-toko'],
            ['name' => 'Customer', 'email' => 'customer@demo.com', 'role' => 'com-customer'],
            ['name' => 'Reseller', 'email' => 'reseller@demo.com', 'role' => 'com-reseller'],
            ['name' => 'Dropshipper', 'email' => 'dropshipper@demo.com', 'role' => 'com-dropshipper']
        ];

        foreach($users as $row) {
            
            $user = User::where('email', $row['email'])->first();
            
            if($user) {
                
                $this->command->info('User ' . $row['name'] . ' already exists.');
            
            } else {
                
                $newUser = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'password' => Hash::make('secret'),
                    'email_verified_at' => Carbon::now(),
                    'is_active' => true,
                    'created_by' => config('setting.db_sysadmin_uuid')
                ]);

                $newUser->assignRole($row['role']);

                $this->command->info('User ' . $row['name'] . ' with role ' . $row['role'] . ' created successfully.');
            
            }
            
            $this->command->info(' All User granted successfully.');
        }

    }
}
