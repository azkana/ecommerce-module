<?php

namespace Modules\Ecommerce\Entities;

use App\Traits\Uuid as TraitsUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductCategory extends EcommerceModel
{
    use HasFactory, TraitsUuid, SoftDeletes;

    protected $table = EcommerceModel::DB_TABLE_PREFIX . 'product_categories';

    protected $fillable = [
        'name', 'slug', 'description', 'parent_id', 'image'
    ];
    
    
}
