<?php

namespace Modules\Ecommerce\Entities;

use App\Models\BaseModel;

class EcommerceModel extends BaseModel
{
    public const DB_TABLE_PREFIX = 'ecom_';
}
