@can('ecommerce')
<li class="treeview @if($adminActiveMenu == 'ecommerce') active @endif">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>eCommerce</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('ecommerce-dashboard-read')
        <li class="@if($adminActiveSubMenu == 'dashboard') active @endif">
            <a href="{!! route('ecommerce.dashboard') !!}">
                <i class="fa fa-circle-o"></i> Unit Operation Schedule
                <span class="pull-right-container"></span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan